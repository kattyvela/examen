//
//  DetailCardViewController.swift
//  Examen
//
//  Created by Norma Reyes on 15/6/17.
//  Copyright © 2017 Katherine Vela. All rights reserved.
//

import UIKit

class DetailCardViewController: UIViewController {

    @IBOutlet weak var imageCard: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var rarityLabel: UILabel!
    
    @IBOutlet weak var elixirLabel: UILabel!
    
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var idName:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let back = BackEndManager();
        back.getDataCardByIdName(idName, completionHandler: { (cardDetail) in
            DispatchQueue.main.async {
                self.loadData(cardDetail)
            }
        })
        
        back.getImageCard(idName) { (imagen) in
            DispatchQueue.main.async {
                self.imageCard.image = imagen
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.idName = idName!
    }
    
    func loadData(_ cardDetail: NSDictionary){
        self.nameLabel.text = cardDetail["name"] as? String
        self.rarityLabel.text = cardDetail["rarity"] as? String
    
        let exilirCost = cardDetail["elixirCost"] as? Int
        self.elixirLabel.text = String(describing: exilirCost!)
       
        self.typeLabel.text = cardDetail["type"] as? String
        self.descriptionLabel.text = cardDetail["description"] as? String
    
    }
    
    @IBAction func saveCard(_ sender: Any) {
        
        let nombre:String = (self.nameLabel.text)?.replacingOccurrences(of: " ", with: "%20") ?? ""
        let rarity:String = self.rarityLabel.text ?? ""
        let exilirCost:String = self.elixirLabel.text ?? ""
        let type:String = self.typeLabel.text ?? ""
        let description:String = self.descriptionLabel.text ?? ""
        let urlImage:String = "http://www.clashapi.xyz/images/cards/\(idName!).png"
        
        let back = BackEndManager()
        back.insertData(name: nombre, rarity: rarity, exilir_cost: exilirCost, type: type, description: description, urlImage: urlImage)
        
        let alert = UIAlertView(title: "Mensaje", message: "Registro Exitoso", delegate:self,cancelButtonTitle: "Ok :)")
        alert.tag = 1
        alert.show()
        
        
    }

    @IBAction func backToCards(_ sender: Any) {
        performSegue(withIdentifier: "toCardsSegue",
                     sender: self)
    }
}
