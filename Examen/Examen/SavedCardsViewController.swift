//
//  SavedCardsViewController.swift
//  Examen
//
//  Created by Norma Reyes on 16/6/17.
//  Copyright © 2017 Katherine Vela. All rights reserved.
//

import UIKit

class SavedCardsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var cardsSavedTable: UITableView!
    
    var savedCards:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let back = BackEndManager()
        back.getSaveCards(completionHandler: { (respuesta) in
            self.savedCards = respuesta
            self.cardsSavedTable.reloadData()
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return savedCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "savedCardCell") as! TableViewCell
        
        let card = savedCards[indexPath.row]as? NSDictionary
        
        let cardName = card?["name"] as? String
        
        let cardRarity = card?["rarity"] as? String
        
        let urlImage = card?["url-image"] as? String
        
        cell.name = cardName?.replacingOccurrences(of: "%20", with: " ")
        cell.rarity = cardRarity
        cell.urlImage = urlImage
        
        cell.fillData()
        
        return cell
    }

    @IBAction func BackToTableCards(_ sender: Any) {
        performSegue(withIdentifier: "ToMainViewSegue",
                     sender: self)
    }

}
