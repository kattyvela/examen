//
//  TableViewCell.swift
//  Examen
//
//  Created by Katherine Vela on 15/6/17.
//  Copyright © 2017 Katherine Vela. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {


    @IBOutlet weak var imageSaved: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var rarityLabel: UILabel!
    
    var imagen:UIImage!
    var name:String!
    var rarity:String!
    var urlImage:String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(){
        
        nameLabel.text = name
        rarityLabel.text = rarity
        
        if imagen == nil {
            let back = BackEndManager()
            back.getImageSaveCards(urlImage!, completionHandler: { (imageR) in
                DispatchQueue.main.async {
                    self.imageSaved.image = imageR
                    
                }
            })
            
        } else {
            imageSaved.image = imagen
        }
        
    }


}
