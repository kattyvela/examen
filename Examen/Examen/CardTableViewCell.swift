//
//  CardTableViewCell.swift
//  Examen
//
//  Created by Norma Reyes on 14/6/17.
//  Copyright © 2017 Katherine Vela. All rights reserved.
//

import UIKit

class CardTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imageCard: UIImageView!
  
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var rarityLabel: UILabel!
    
    var imagen:UIImage!
    var name:String!
    var rarity:String!
    var urlImage:String!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func fillData(){
        
        nameLabel.text = name
        rarityLabel.text = rarity
        
        if imagen == nil {
            
            let back = BackEndManager()
            back.getImageCard(urlImage!, completionHandler: { (imageR) in
                DispatchQueue.main.async {
                    self.imageCard.image = imageR
                    
                }
            })
            
        } else {
            
            imageCard.image = imagen
        }
        
    }

}


