//
//  BackEndManager.swift
//  Examen
//
//  Created by Norma Reyes on 14/6/17.
//  Copyright © 2017 Katherine Vela. All rights reserved.
//

import Alamofire
import AlamofireImage
import AlamofireObjectMapper
import Foundation

class BackEndManager {
    
    
    func getDataCards(completionHandler: @escaping (NSArray)-> Void) {
        let urlString = "http://www.clashapi.xyz/api/cards"
        
        Alamofire.request(urlString).responseJSON {
            response in
            guard let JSON = response.result.value as? NSArray else{
                return
            }
            DispatchQueue.main.async {
                completionHandler(JSON)
            }
        }
        
    }
    
    
    func getImageCard(_ idName:String, completionHandler: @escaping(UIImage)->()){
        
        let url = "http://www.clashapi.xyz/images/cards/\(idName).png"
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
        
    }
    
    
    func getDataCardByIdName(_ idName: String, completionHandler: @escaping (NSDictionary)-> Void) {
        
        let urlString = "http://www.clashapi.xyz/api/cards/\(idName)"

        Alamofire.request(urlString).responseJSON {
            response in
            guard let JSON = response.result.value as? NSDictionary else{
                return
            }
            
            completionHandler(JSON)
        }
        
    }
    
    func insertData (name:String, rarity:String, exilir_cost:String, type:String, description:String, urlImage:String ) {
        
        let urlString = "https://examenios.mybluemix.net/insert?name=\(name)&rarity=\(rarity)&exilirCost=\(exilir_cost)&type=\(type)&description=nothing&url-image=\(urlImage)"
        
        Alamofire.request(urlString).responseJSON {
            response in
            
            
        }
        
    }
    
    
    func getSaveCards(completionHandler: @escaping (NSArray)-> Void) {
        
        let urlString = "https://examenios.mybluemix.net/get"
        
        Alamofire.request(urlString).responseJSON {
            response in
            guard let JSON = response.result.value as? NSArray else{
                return
            }
            DispatchQueue.main.async {
                completionHandler(JSON)
            }
            
        }
    }
    
    
    func getImageSaveCards(_ urlImage:String, completionHandler: @escaping(UIImage)->()){
        
        
        Alamofire.request(urlImage).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
        
    }



    
}
