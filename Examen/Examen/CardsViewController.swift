//
//  CardsViewController.swift
//  Examen
//
//  Created by Norma Reyes on 15/6/17.
//  Copyright © 2017 Katherine Vela. All rights reserved.
//

import UIKit

class CardsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var cardsTable: UITableView!

    var cards:NSArray = []
    
    var idName:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let back = BackEndManager()
        back.getDataCards { (respuesta) in
            self.cards = respuesta
            self.cardsTable.reloadData()
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell") as! CardTableViewCell
        
        let card = cards[indexPath.row]as? NSDictionary
        
        let cardName = card?["name"] as? String
        
        let cardRarity = card?["rarity"] as? String
        
        let idName = card?["idName"] as? String
        
        let urlImage = idName
        
        cell.name = cardName
        cell.rarity = cardRarity
        cell.urlImage = urlImage
        
        cell.fillData()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let card = cards[indexPath.row]as? NSDictionary
        idName = card?["idName"] as? String
        
        performSegue(withIdentifier: "toDetailSegue",
                     sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailSegue" {
            let destination = segue.destination as! DetailCardViewController
            destination.idName = self.idName;
        }
    }


    @IBAction func toSavedCards(_ sender: Any) {
        performSegue(withIdentifier: "toSavedCardsSegue",
                     sender: self)
    }
   


}
