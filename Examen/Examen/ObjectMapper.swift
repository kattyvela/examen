//
//  ObjectMapper.swift
//  Examen
//
//  Created by Norma Reyes on 15/6/17.
//  Copyright © 2017 Katherine Vela. All rights reserved.
//

import Foundation
import ObjectMapper

class CardsResult:Mappable{
    var resultados:[CardDetail]?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        resultados <- map["SUCCESS"]
    }
}

class CardDetail:Mappable {
    
    var idName:String?
    var name:String?
    var rarity:String?
    var elixirCost:Int?
    var type:String?
    var description:String?
    var imagen:UIImage?
    
    required init(map:Map) {
        
    }
    
    func mapping(map: Map) {
        idName <- map["idName"]
        name <- map["name"]
        rarity <- map["rarity"]
        elixirCost <- map["elixirCost"]
        type <- map["type"]
        description <- map["description"]
    }
    
}


