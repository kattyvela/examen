//
//  TabBarController.swift
//  Examen
//
//  Created by Norma Reyes on 14/6/17.
//  Copyright © 2017 Katherine Vela. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let items = tabBar.items
        items?[0].title = "CARDS"
        items?[1].title = "SAVED CARDS"
    }

    

  }
